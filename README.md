# README #

Download this code and build it.

# Input file #

The input file "sourceData.txt" stores all the path data.

Each line is in the following format

<Path name>:<departure time>:<departure location>:<arrival time>:<arrival location>:<plane type>:<cost>

e.g.
FS1:SYD:370:AKL:660:B787:10404

# Output file #

A file "output.txt" will be generated in the src folder.