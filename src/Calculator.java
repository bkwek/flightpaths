import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import data.*;

public class Calculator {

	static final int TURNAROUND_TIME = 120;
	
	static Map<String, List<Path>> locations;
	static List<List<Path>> routes;
	static List<Path> paths;
	
	public static void main(String[] args) throws IOException {
		populateLocationsAndPaths(); 	// Set up the paths 
		calculateOneDayRoutes();		// Calculate and store 1-day routes
		calculateMultiDayRoutes(3);		// Connect 1-day routes into multi-day routes
		writeOutResults("output.txt");	// write data out
	}
	
	private static void writeOutResults(String outputFile) throws IOException {
		BufferedWriter outWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/" + outputFile)));
		
		for (String s : printedRoutes) {
			outWriter.write(s);
			outWriter.newLine();
		}

		outWriter.flush();
		outWriter.close();
	}
	
	private static void populateLocationsAndPaths() throws IOException {
		locations = new HashMap<>();
		paths = new ArrayList<>();
		
		// Start reading the file
		BufferedReader inReader = new BufferedReader(new InputStreamReader(new FileInputStream("src/sourceData.txt")));
		
		String tmp;
		
		// for each line in the file
		while ((tmp = inReader.readLine()) != null) {
			String[] inputLine = tmp.split(":");

			// store the path data
			Path p = new Path(inputLine[2], inputLine[4], inputLine[3], inputLine[0], inputLine[6], inputLine[5], inputLine[1]);
			paths.add(p);	
		}
		
		inReader.close();
	}
	
	private static void calculateOneDayRoutes() {
		routes = new ArrayList<>();
		
		// for each path
		for (Path p : paths) {
			List<Path> flights = new ArrayList<>();
			
			flights.add(p);
			// recursively add new paths to the current route
			traversePath(p, p.arrivalTime, flights);
		}
	}
	
	private static void traversePath(Path source, int currentTime, List<Path> flights) {
		// for each other path
		for (Path p : paths) {
			if (p != source && 												
				p.source.equals(source.destination) && 					// if this path's source is the previous path's destination
				p.planeType.equals(source.planeType) && 				// if this path's plane is the same as the previous path's plane type
				p.departureTime >= source.arrivalTime + TURNAROUND_TIME	// if this path's departure time is later than the previous path's arrival time + turnaround
			) {
				// we can add this path to the route
				flights.add(p);
				
				// and recursively add new paths to the current route
				traversePath(p, p.arrivalTime, flights);
			}
		}
		
		// we will get here once there are no more paths to traverse (recursion ends)
		
		// if there are more than 1 paths on the route
		if (flights.size() > 1) {
			
			// add it as a 1-day route
			List<Path> tmp = new ArrayList<>();
			tmp.addAll(flights);
			routes.add(tmp);	
		}
		
		// remove this path from the current route
		flights.remove(flights.size() - 1);
	}
	

	
	private static void calculateMultiDayRoutes(int numDays) {
		List<Path> holder = new ArrayList<>();
		
		for (List<Path> r : routes) {
			holder.addAll(r);
			connectPaths(r.get(r.size()-1).destination, 1, numDays, holder);

			for (int i = 0; i < r.size(); i++) {
				holder.remove(holder.size()-1);
			}
		}
		
		System.out.println("printed count: " + printedRoutes.size());
	}
	
	private static String printRoute(List<Path> path) {
		String str = "";
		
		for (Path p : path) {
			str += p.name + "-";
		}
		
		return str;
	}
	
	static List<String> printedRoutes = new ArrayList<>();
	
	private static void connectPaths(String lastLocation, int depth, int targetDepth, List<Path> holder) {
		if (depth == targetDepth) {
			if (isValidRoute(holder)) {
				String printedRoute = printRoute(holder);
				printedRoutes.add(printedRoute);
			}
			return;
		}
		
		for (List<Path> r : routes) {
			
			if (r.get(0).source.equals(lastLocation)) {
				holder.addAll(r);
				connectPaths(r.get(r.size()-1).destination, depth+1, targetDepth, holder);
				
				for (int i = 0; i < r.size(); i++) {
					holder.remove(holder.size()-1);
				}
			}
		}
	}
	
	private static boolean isValidRoute(List<Path> route) {
		
		Path firstPath = route.get(0);
		Path lastPath = route.get(route.size() - 1);
		
		if (!lastPath.destination.equals(firstPath.source)) return false;
		
		for (Path p : route) {
			if (p.source.equals(p.destination)) return true;
		}
		
		return false;
	}

}
